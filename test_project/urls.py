from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.contrib import admin
from apps.profile.views import ProfileView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'test_project.views.home', name='home'),
    # url(r'^test_project/', include('test_project.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^my-profile/$', login_required(TemplateView.as_view(template_name='profile/profile.html')), name="profile"),
    url(r'^my-profile/edit/$', login_required(ProfileView.as_view()), name="profile_edit"),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name="auth_login"),
    (r'^my_admin/jsi18n', 'django.views.i18n.javascript_catalog'),

)
