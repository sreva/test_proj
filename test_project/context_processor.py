from  django.conf import settings


def SettingsContext(request):
    return {"settings": settings}

