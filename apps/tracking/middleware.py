from datetime import datetime
from .models import DBRequestsLog


class HttpRequests(object):
    def process_request(self, request):
        DBRequestsLog.objects.create(
            timestamp=datetime.now(),
            method=request.META.get('REQUEST_METHOD', 'GET'),
        )




