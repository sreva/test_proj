from django.db import models

# Create your models here.


class SignalsLog(models.Model):
    action_choices = (
        (1, 'Create'),
        (2, 'Change'),
        (3, 'Delete')
    )

    model = models.CharField(max_length=50)
    instance = models.CharField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add=True)
    action = models.IntegerField(max_length=1, choices=action_choices)
    class Meta:
        verbose_name = "SignalsLog"
        verbose_name_plural = "SignalsLogs"


class DBRequestsLog(models.Model):
    timestamp = models.DateTimeField('Request date/time')
    method = models.CharField('Method', max_length=10)


    class Meta:
        verbose_name = "DBRequestsLog"
        verbose_name_plural = "DBRequestsLogs"