from signals import signals_logger, signals_logger_delete

from django.db.models.signals import post_save
from django.db.models.signals import post_delete


post_save.connect(signals_logger)
post_delete.connect(signals_logger_delete)