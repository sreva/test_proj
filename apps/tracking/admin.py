
from .models import DBRequestsLog
from .models import SignalsLog
from django.contrib import admin




admin.site.register(SignalsLog)
admin.site.register(DBRequestsLog)