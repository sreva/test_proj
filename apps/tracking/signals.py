from .models import SignalsLog
exclude = (
    'SignalsLog',
    'DBRequestsLog',
    'LogEntry',
)

def signals_logger(sender, **kwargs):
    if sender._meta.object_name in exclude:
        return False
    SignalsLog.objects.create(
        model=sender._meta.object_name,
        instance=unicode(kwargs.get('instance')),
        action=1 if kwargs.get('created') else 2)


def signals_logger_delete(sender, **kwargs):
    if sender._meta.object_name in exclude:
        return False
    SignalsLog.objects.create(
        model=sender._meta.object_name,
        instance=unicode(kwargs.get('instance')),
        action=3)
