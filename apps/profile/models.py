from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
# Create your models here.


class User(AbstractUser):
    data_of_birth = models.DateField(null=True, blank=True)
    biography = models.TextField(blank=True)
    contacts = models.TextField(blank=True)
    #Manager
    objects = UserManager()
    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'