from django import forms
from .models import User
from django.contrib.admin import widgets


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('contacts', 'biography', 'data_of_birth', 'last_name', 'first_name')
        exclude = ('username',)

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['data_of_birth'].widget = widgets.AdminDateWidget()
