from django.views.generic import View
from django.shortcuts import render_to_response
from django.template import RequestContext
from .forms import UserForm

class ProfileView(View):
    def get(self, request, **kwargs):
        form = UserForm(instance=request.user)
        return render_to_response('profile/profile_edit.html',{'form':form}, context_instance = RequestContext(request))

    def post(self, request, **kwargs):
        form = UserForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
        return render_to_response('profile/profile_edit.html',{'form':form}, context_instance = RequestContext(request))
