from django.contrib.auth.admin import UserAdmin
from .models import User
from django.contrib import admin


class AdminUser(UserAdmin):
    pass

admin.site.register(User, AdminUser)